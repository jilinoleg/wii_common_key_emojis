# Wii Common Key Emojis for Fediverse

Fediverse emoji pack with [completely random colors] image flag. Show everyone that you are true [perfectly legal buyer] of retro games

## Install (Completely not tested for now, may completely not work)

Pleroma/akkoma:

```
mix pleroma.emoji get-packs wii_common_key_emojis -m https://codeberg.org/jilinoleg/wii_common_key_emojis/raw/branch/main/manifest.json
```

Mastodon/Glitchsoc

Use Mastodon Emoji importer with zip file from release

## Build
run export.sh script in Linux machine with `bash`, `optipng` and `inkscape` installed

then fix manifest and the other json file, and manually pack into zip

## License
This work is licensed under CC-BY-4.0

You may say "[+f7] you" and ignore legal crediting things, but please credit https://github.com/alarixnia/pride-hearts/tree/master for heart icon (originally png), and https://www.svgrepo.com/collection/iconhub-glyph-icons/ for verified svg icon if you are going to use it outside of fedi emojis

Manifest and installation is stolen from Volpeon, pat dis wyvern fox dragon thing if you meet
