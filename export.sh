#!/bin/bash

rm -rf ./export

inkscape $(find . | grep .svg) --export-type=png --export-width=512 --export-png-color-mode=RGBA_16 
optipng $(find . | grep .png)

mkdir export
mv -- *.png ./export
